const Post = require("../models/postsModel");

//CREATE, UPDATE, DELETE, READ ALL, READ ONE

const createPost = async (req, res) => {
  try {
    const { title, body } = req?.body || {};
    if (!title || !body) return res.status(400).json({ message: "Please provide all the fields" });
    const post = new Post({
      title: title,
      body: body,
    });

    await post.save();

    res.status(201).json({
      message: "Post created successfully",
      post: post,
    });
  } catch (err) {
    res.status(500).json({
      message: "Something went wrong",
      error: err,
    });
  }
};

module.exports = {
  createPost,
};
