const express = require("express");
const dotenv = require("dotenv");
const postRoutes = require("./routes/postRoutes");
const connectDB = require("./config/db");

//Initialize the app
const app = express();
app.use(express.json());

//Initialize the dotenv
dotenv.config();

//Set up the port
const PORT = process.env.PORT || 8080;

connectDB(
  "mongodb+srv://" +
    process.env.MONGO_USER +
    ":" +
    encodeURIComponent(process.env.MONGO_PSW) +
    "@" +
    process.env.MONGO_URI
);

//Set up the main route
app.get("/", (req, res) => {
  res.send("Hello World");
});

//Set up the dice route
app.use("/post", postRoutes);

//Listen to the port
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
